import { Component } from '@angular/core';
import { InAppBrowser,InAppBrowserObject,InAppBrowserEvent  } from '@ionic-native/in-app-browser/ngx';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser/ngx';
import { Platform } from '@ionic/angular';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public unsubscribeBackEvent: any;
  constructor(private iab: InAppBrowser, private themeableBrowser: ThemeableBrowser, private platform: Platform) { }

  ngOnInit() {
    this.initializeBackButtonCustomHandler();
    this.openBlank();
  }

  initializeBackButtonCustomHandler(): void {
    this.unsubscribeBackEvent = this.platform.backButton.subscribeWithPriority(999999,  () => {
      console.log("back pressed home!!!!!!!!!!!!!");
        alert("back pressed home" + this.constructor.name);
    });
  }

   //Called when view is left
   ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unsubscribeBackEvent && this.unsubscribeBackEvent();
  }

  openBlank() {

    const options: ThemeableBrowserOptions = {
    
      backButtonCanClose:false,
      toolbar: {
        height: 44,
        color: '#3573bbff'
      },
      title: {
        color: '#ffffffff',
        showPageTitle: true,
        staticText: 'Almanhal Application'
      },
     
      closeButton: {
        wwwImage: 'assets/img/logout.png',
        align: 'left',
        event: 'closePressed'
      },
     
    };

    const browser: ThemeableBrowserObject = this.themeableBrowser.create('https://tasjeel-almanhal.moe.gov.ae/?locale=default', '_blank', options);

    

 
    browser.on('closePressed').subscribe(data => {
      browser.close();
    });

    /*
    browser.on('loadstop').subscribe(data => {
      browser.close();
    });
    */

   if (browser.on('loadstop')) {
    browser.on('loadstop').subscribe((ev: InAppBrowserEvent) => {
      console.log("!!!!!!!!!!!!ev.url*******>>>>>>> loadstop fired !!!!!!   "+ev.url);
      console.log("!!!!!!!!!!!!ev.url*******>>>>>>> loadstop fired !!!!!!   "+ev.type);
      console.log("!!!!!!!!!!!!ev.url*******>>>>>>> loadstop fired !!!!!!   "+ev.code);
      console.log("!!!!!!!!!!!!ev.url*******>>>>>>> loadstop fired !!!!!!   "+ev.message);
        // do whatever is needed here, for example check the url of the browser event
        if (ev.url) {
          
         /// browser.executeScript({ code: "document.querySelector('body > div:nth-child(6) > div:nth-child(2) > div > div.z-ContentArea > div > div > div:nth-child(1) > div.z-PortalViewPanel.z-LoginBox.z-Animation.GEKGWJ3OEC > div.z-FlowHorizontalPanel.GEKGWJ3EEC.z-RegLoginSignupPanel > div.z-FlowVerticalPanel.z-RegLoginPanel.z-RegLoginPanelStyle > div > div:nth-child(1) > div > div.z-FlowVerticalPanel > table:nth-child(1) > tbody > tr:nth-child(2) > td > input').value = 'msajith@gmail.com';" });

      browser.executeScript({ code: "var element = document.evaluate('\/html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[1]/div[2]/div[2]/div/div[1]/div/div[1]/table[1]/tbody/tr[2]/td/input\' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;if (element != null) { element.value = '\msajith@gmail.com'\;}"});

      browser.executeScript({code: "document.evaluate('\/html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[1]/div[2]/div[2]/div/div[1]/div/div[1]/table[1]/tbody/tr[2]/td/input\', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue"}).then(data=>{
            console.log("111@@@@@!!!!!!!!!!!!!script executed >>>>>  "+data);
           },err=>{
             console.log(err,"!!!!!!!!!!!error in executing the script");
           }
       );

       browser.executeScript({code: "document.title"}).then(data=>{
        console.log("222@@@@@!!!!!!!!!!!!!script executed >>>>>  "+data);
       },err=>{
         console.log(err,"!!!!!!!!!!!error in executing the script");
       }
   );

          
            // do stuff based on url and url parameters
            // in our solution we got an order id from a confirmation page
            //console.log("!!!!!!!!!!!!ev.url*******>>>>>>>  "+ev.url);
        }
        // for the event to trigger we added a hidden button to interact with the screen
      
    });
}


    

    /*
    const browser: InAppBrowserObject  = this.iab.create(`https://jamibot.com`, `_blank`);
    console.log("openBlank starting -->>> ");
    if (browser.on('loadstop')) {
      browser.on('loadstop').subscribe((ev: InAppBrowserEvent) => {
          // do whatever is needed here, for example check the url of the browser event
          if (ev.url) {
              // do stuff based on url and url parameters
              // in our solution we got an order id from a confirmation page
              console.log("ev.url -->>> "+ev.url);
          }
          // for the event to trigger we added a hidden button to interact with the screen
          //btn.click();
      });
  }
  */
}

}
